CC := gcc
CFILES := tri.c
CFLAGS := -g -std=c99

all: tri

tri: $(CFILES:.c=.o)

%.o: %.c %.h
	gcc $(CFLAGS) $<

# vim:set noet sts=0 sw=4 ts=4 tw=120:

