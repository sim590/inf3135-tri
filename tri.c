/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define HELP "\
tri -- Outil de tri\n\
\n\
SYNOSPIS\n\
    tri [-h] [-r] [-b] < file\n\
\n\
DESCRIPTION\n\
    Tri le contenu du fichier au format JSON passé à l'entrée standard. \n\
OPTIONS\n\
    -h, --help\n\
        Affiche de l'aide à la sortie standard et quitte.\n\
    -r, --rapid\n\
        Exécute le tri rapide.\n\
    -b, --binary\n\
        Exécute le tri binaire.\n\
"

struct parsed_args {
    bool fail;
    bool help;
    bool rapid;
    bool binary;
};

static const struct option long_options[] = {
   {"help",           no_argument,       NULL, 'h'},
   {"rapid",          no_argument,       NULL, 'r'},
   {"binary",         no_argument,       NULL, 'b'},
   {NULL,             0,                 NULL,  0 }
};

struct parsed_args parse_args(int argc, char *argv[]) {
    struct parsed_args pa = {0};
    int opt;
    while ((opt = getopt_long(argc, argv, "hrb", long_options, NULL)) != -1) {
        switch (opt) {
        case 'h':
            pa.help = true;
            break;
        case 'r':
            pa.rapid = true;
            break;
        case 'b':
            pa.binary = true;
            break;
        default:
            pa.fail = true;
            return pa;
        }
    }
    return pa;
}

int main(int argc, char *argv[]) {
    struct parsed_args pa = parse_args(argc, argv);
    if (pa.fail) {
        return EXIT_FAILURE;
    } else if (pa.help) {
        printf("%s\n", HELP);
        return EXIT_SUCCESS;
    }

    /* TODO: écrire le code. */
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

